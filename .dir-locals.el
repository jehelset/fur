;;assumes cog is built and checked out in same directory as fur
((nil
  (eval . (progn
            (setq cog-dir (concat (projectile-project-root) "../cog/"))
            (setq cog-bin-dir (concat (file-name-as-directory cog-dir) "tmp/stage/bin"))
            (setq cog-lib-dir (concat (file-name-as-directory cog-dir) "tmp/stage/lib"))
            (setenv "PATH" (concat cog-bin-dir ":" (getenv "PATH")))
            (setenv "LD_LIBRARY_PATH" (concat cog-lib-dir ":" (getenv "LD_LIBRARY_PATH")))))
  (eval . (progn
            (setq fur-dir (projectile-project-root))
            (setq fur-bin-dir (concat (file-name-as-directory fur-dir) "tmp/stage/bin"))
            (setq fur-lib-dir (concat (file-name-as-directory fur-dir) "tmp/stage/lib"))
            (setenv "PATH" (concat fur-bin-dir ":" (getenv "PATH")))
            (setenv "LD_LIBRARY_PATH" (concat fur-lib-dir ":" (getenv "LD_LIBRARY_PATH")))))
  ))

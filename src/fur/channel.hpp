#pragma once
#include<fur/preamble.hpp>
namespace fur{

  namespace channel{
    using native_ = Display *;
    namespace native{ constexpr native_ none = None; }
    using request_ = const char *;
    namespace request{ constexpr request_ none=nullptr; }

    using socket = int;

    struct init{ init(); }; }

  struct channel_{
    static const channel::init init;
    
    channel::native_ native;

    operator channel::native_()const noexcept{ return native; }

    channel_(channel::request_ = channel::request::none);
    channel_(channel_ &&);
    channel_ &operator=(channel_ &&);
    ~channel_();

    channel_(const channel_ &)=delete;
    channel_ &operator=(const channel_ &)=delete;

    channel::socket socket()const;
    void flush()const;
    std::size_t pending()const; };
    
}

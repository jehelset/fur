#pragma once
#include<functional>
#include<fur/geometry/preamble.hpp>
#include<fur/geometry/reactor.hpp>
namespace fur::geometry{

namespace object_{
  namespace guts{
    template<class category> struct value_impl;
    template<> struct value_impl<category::cst>{ using type=field; };
    template<> struct value_impl<category::ext>{ using type=field; };
    template<unop_c unop> struct value_impl<unop>{ using type=id; };
    template<binop_c binop> struct value_impl<binop>{ using type=ids; };
  }

  template<class category>
  using value_ = typename guts::value_impl<category>::type;

  using value =
    decltype(meta::unpack(
      meta::transform(categories,meta::template_<value_>),
      meta::template_<std::variant>))::type;
      
}
struct object{
  object::value value;
  ids dep;

  reactor::domain_t reactor;  
};
using objects = std::vector<object> ; 

template<class T>
concept bool objects_c = std::is_same_v<std::remove_cvref_t<T>,objects_t>;

template<class ideal_m,objects_c objects_t>
constexpr decltype(auto) at(objects_t &&o)noexcept{
  return std::get<std::vector<object_tt<ideal_m>>>(std::forward<objects_t>(o));
}
template<objects_c objects_t,class ideal_m>
constexpr decltype(auto) at(objects_t &&o,const id_tt<ideal_m> &oi){
  return at<ideal_m>(std::forward<objects_t>(o)).at(oi.index);
}

template<objects_c objects_t>
decltype(auto) dep(objects_t &&o,const id_t &id){
  return std::visit([&](const auto &id)->auto &{ return at(std::forward<objects_t>(o),id).dep; },id);
}
template<objects_c objects_t>
decltype(auto) rea(objects_t &&o,const id_t &id){
  return std::visit([&](const auto &id)->auto &{ return at(std::forward<objects_t>(o),id).reactor; },id);
}
  
namespace object{
  #define FUR_MACRO(id) using id##_t=object_tt<id##_m>;
  #include<fur/geometry/ideal.mpp>
  #undef FUR_MACRO
}

template<class ideal_m,class... R>
void react(object_tt<ideal_m> &o,R &&... r){
  (reactor::add(o.reactor,std::forward<R>(r)),...);
}

}

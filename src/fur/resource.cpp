#include<iostream>
#include<fur/resource.hpp>
namespace fur::resource{

namespace event{

  constexpr auto to_native_type =
    [](auto event){
      constexpr auto map =
        meta::make_map(meta::make_pair(categories::configure,meta::int_c<ConfigureNotify>),
                       meta::make_pair(categories::show,meta::int_c<MapNotify>),
                       meta::make_pair(categories::hide,meta::int_c<UnmapNotify>),
                       meta::make_pair(categories::key_press,meta::int_c<KeyPress>),
                       meta::make_pair(categories::key_release,meta::int_c<KeyRelease>),
                       meta::make_pair(categories::pointer_motion,meta::int_c<MotionNotify>),
                       meta::make_pair(categories::button_press,meta::int_c<ButtonPress>),
                       meta::make_pair(categories::button_release,meta::int_c<ButtonRelease>),
                       meta::make_pair(categories::enter_window,meta::int_c<EnterNotify>),
                       meta::make_pair(categories::leave_window,meta::int_c<LeaveNotify>),
                       meta::make_pair(categories::expose,meta::int_c<Expose>));  
      return meta::at_key(map,event); }; 

  constexpr auto to_native_member_ptr =
    meta::overload(
      [](category::configure){ return &native::any::xconfigure; },
      [](category::show){ return &native::any::xmap; },
      [](category::hide){ return &native::any::xunmap; },
      [](category::key_press){ return &native::any::xkey; },
      [](category::key_release){ return &native::any::xkey; },
      [](category::pointer_motion){ return &native::any::xmotion; },
      [](category::button_press){ return &native::any::xbutton; },
      [](category::button_release){ return &native::any::xbutton; },
      [](category::enter_window){ return &native::any::xcrossing; },
      [](category::leave_window){ return &native::any::xcrossing; },
      [](category::expose){ return &native::any::xexpose; });

  template<class category>
  void demux_(const native::any &na,std::optional<any> &ev){
    if(to_native_type(meta::type_c<category>)() == na.type)	
      ev = event_<category>{na.*to_native_member_ptr(category{})}; }

  std::optional<any> demux(const native::any &na){
    constexpr auto impls =
      meta::unpack(
        category::all,
        [&](auto... category)
          -> std::array<void(*)(const native::any &,std::optional<any> &),sizeof...(category)>
        { return {{&demux_<typename decltype(category)::type>...}}; } );
    std::optional<any> ev{};
    ranges::find_if(impls,[&](auto impl){ impl(na,ev); return ev.has_value(); });
    return ev; } 

  std::vector<any> read(channel_ &c){
    std::size_t ev_n=c.pending();
    std::vector<any> evs;
    native::any ev_native,ev_native_next;
    for(std::size_t i=0;i!=ev_n;++i){
      XNextEvent(c,&ev_native);
      if(ev_native.type == to_native_type(categories::pointer_motion)())
        for(std::size_t j=i+1;j!=ev_n;++j){
          XPeekEvent(c,&ev_native_next);
          if(ev_native_next.type != to_native_type(categories::pointer_motion)())
            break;
          XNextEvent(c,&ev_native);
          i=j; }

      std::optional<any> ev=demux(ev_native);
      if(!ev)
        continue;
      evs.push_back(std::move(*ev)); }
    return evs; } }


native::attrs attrs(const object::window &o){
  native::attrs a;
  XGetWindowAttributes(o.channel,o.native.window,&a);
  return a; }

void select(object::window &o,long em){ XSelectInput(o.channel,o.native.window,em); }

void show(object::window &o){ XMapWindow(o.channel,o.native.window); }

void hide(object::window &o){ XUnmapWindow(o.channel,o.native.window); }

void move(object::window &o,double x,double y){ XMoveWindow(o.channel,o.native.window,(int)x,(int)y); }

void resize(object::window &o,double w,double h){
  if(w<=1||h<=1)
    return;
  XResizeWindow(o.channel,o.native.window,(unsigned int)w,(unsigned int)h); }

void border(object::window &o,const color &c){ XSetWindowBorder(o.channel,o.native.window,c.pixel()); }

void background(object::window &o,const color &c){ XSetWindowBackground(o.channel,o.native.window,c.pixel()); }

void clear(object::window &o){ XClearArea(o.channel,o.native.window,0,0,0,0,False); }

void swap(object::glx_window &gw){ glXSwapBuffers(gw.channel,gw.native.glx_window); }
void current(object::glx_window &gw){ glXMakeCurrent(gw.channel,gw.native.glx_window,gw.native.glx_context); }
void release(object::glx_window &gw){ glXMakeContextCurrent(gw.channel,None,None,nullptr); }

std::size_t screen_count(const channel_ &ch){ return XScreenCount(ch); }
std::size_t default_screen(const channel_ &ch){ return XDefaultScreen(ch); }    

namespace native{

  using glx_config_ = GLXFBConfig;

  glx_config_ glx_config(channel_ &c,std::size_t s){
    std::vector<int> glx_config_attr{
      GLX_RED_SIZE,4,
      GLX_GREEN_SIZE,4,
      GLX_BLUE_SIZE,4,
      GLX_ALPHA_SIZE,4,
      GLX_DOUBLEBUFFER,1,
      None };
    int nconfigs;
    glx_config_ *configs=glXChooseFBConfig(c,s,glx_config_attr.data(),&nconfigs);
    if(nconfigs==0)
      throw std::runtime_error("no config found");
    glx_config_ config=configs[0];
    XFree(configs);
    return config; }

  template<class category>
  native_<category> make(objects &,const request_<category> &){
    return {}; }
    
  native::screen make(objects &c,request::screen &request){
    if(request.screen_number.has_value())
      request.screen_number = default_screen(c.channel);
      
    native::screen native;
    native.screen_number = *request.screen_number;
    native.screen        = XScreenOfDisplay(c.channel,native.screen_number);
    
    {
      XVisualInfo request_native;
      long request_native_mask = VisualScreenMask  
                               | VisualDepthMask
                               | VisualClassMask
                               | VisualRedMaskMask
                               | VisualGreenMaskMask
                               | VisualBlueMaskMask
                               ;
                                 
      request_native.screen     = native.screen_number;
      request_native.depth      = 32;
      request_native.c_class    = TrueColor;
      request_native.red_mask   = 0xFF0000;
      request_native.green_mask = 0xFF00;
      request_native.blue_mask  = 0xFF;
      
      XVisualInfo *match;
      int32_t match_count;
      if(!(match=XGetVisualInfo(c.channel,request_native_mask,&request_native,&match_count)))
        throw std::runtime_error("fur.resource.make.screen");
      native.visual_info=match[0];
      XFree(match); }
      
    native.colormap = XCreateColormap(c.channel,
                                      native.screen->root,
                                      native.visual_info.visual,
                                      AllocNone);
                                      
    {
      glx_config_ gc=glx_config(c.channel,native.screen_number);
      typedef GLXContext (*glx_create_context_t)(Display*, GLXFBConfig, GLXContext, Bool, const int*);      
      glx_create_context_t glx_create_context = (glx_create_context_t) glXGetProcAddress((const GLubyte *)"glXCreateContextAttribsARB");
       if(glx_create_context == nullptr)
        throw std::runtime_error("glXCreateContextAttribsARB function implementation not found\n");

      std::vector<int> glx_context_attr{ 
        GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
        GLX_CONTEXT_MINOR_VERSION_ARB, 3,
        None};
      native.glx_context=glx_create_context(c.channel,gc,0,True,glx_context_attr.data());
      if(native.glx_context==nullptr)
        throw std::runtime_error("fur.resource.make - context creation failed"); }
    return native; }
    
  native::window make(objects &c,request::window &request){
    if(!request.screen.has_value())
      request.screen = c.default_screen;
      
    const object::screen &screen = c.at(*request.screen);
    
    native::raw::window parent; 
    if(request.parent.has_value())
      parent = c.at(*request.parent).native.window;
    else
      parent = screen.native.screen->root;
    
    XSetWindowAttributes native_request;
    native_request.border_pixel = 0;
    native_request.colormap     = screen.native.colormap;
    
    unsigned long native_request_mask=CWBorderPixel|CWColormap;

    return {
      XCreateWindow(
        c.channel,
        parent,
        0,0, //x,y
        640,480, //w,h
        0, //border
        screen.native.visual_info.depth,
        InputOutput,
        screen.native.visual_info.visual,
        native_request_mask,
        &native_request) }; }
        
  native::glx_window make(objects &c,request::glx_window &request){
    auto &parent = c.at(request.window);
    glx_config_ gc = glx_config(c.channel,*parent.request.screen);
    glx_window na{};
    na.glx_window = glXCreateWindow(c.channel,gc,parent.native.window,nullptr); 
    if(na.glx_window == none)
      throw std::runtime_error("fur.resource.make - error");
    auto &screen = c.at(*parent.request.screen);
    na.glx_context = screen.native.glx_context;
    return na; } }

template<class category>
id_<category> objects::make(request_<category> r){
  id_<category> i{at<category>().size()};

  at<category>().push_back({channel,std::move(r),native::make(*this,r)});
  
  meta::overload([&](id::window i){ id[at(i).native.window] = id::any{i}; },
                 [&](auto &){})
                (i);
  return i; }

#define FUR_MACRO(id_,...)\
  template id::id_ objects::make(request::id_);
#include<fur/resource.mpp>
#undef FUR_MACRO

objects::objects(channel_ &ch):
  channel{ch}
{
  const std::size_t screen_n = screen_count(channel);
  for(std::size_t screen_i=0;screen_i!=screen_n;++screen_i)
    make(request::screen{screen_i});
  default_screen = id::screen{resource::default_screen(channel)}; }
  
objects::~objects(){}

}


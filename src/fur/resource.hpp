#pragma once
#include<fur/preamble.hpp>

#define GL_GLEXT_PROTOTYPES
#define GLX_GLXEXT_PROTOTYPES
#include<GL/glx.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<fur/channel.hpp>
namespace fur::resource{

  namespace category{

    namespace guts{
      constexpr index index_offset=__COUNTER__+1;
      template<class> struct at_impl; }

    #define FUR_INDEX __COUNTER__-guts::index_offset
    #define FUR_MACRO(id,...)\
      struct id{};\
      namespace guts{\
        template<>\
        struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
          using type = id; }; }
    #include<fur/resource.mpp>
    #undef FUR_MACRO

    constexpr index count=FUR_INDEX;
    #undef FUR_INDEX

    constexpr auto at = meta::metafunction<guts::at_impl>;

    constexpr auto all = meta::transform(meta::unpack(meta::range_c<index,0,count>,
                                                      meta::make_tuple),
                                         at);

    constexpr auto index = [](auto c){ return meta::index_if(all,meta::equal.to(c)).value(); }; }

  namespace categories{
    #define FUR_MACRO(id,...) constexpr auto id = meta::type_c<category::id>;
    #include<fur/resource.mpp>
    #undef FUR_MACRO
  }
    
  template<class category>
  struct id_{
    index value;
    operator const index &()const noexcept{ return value; } };

  namespace id{
    using any = decltype(meta::unpack(meta::transform(category::all,meta::template_<id_>),
                                      meta::template_<std::variant>))::type; }

  namespace id{
    #define FUR_MACRO(id,...) using id = id_<category::id>;
    #include<fur/resource.mpp>
    #undef FUR_MACRO
  }
  
} 
namespace fur::resource{

namespace event::group{
  namespace guts{
    constexpr index index_offset=__COUNTER__+1;
    template<class> struct at_impl; }
    
  #define FUR_INDEX __COUNTER__-guts::index_offset

  #define FUR_MACRO(id,...)\
    struct id{};\
    namespace guts{\
      template<>\
      struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
        using type=id; }; }
  #include<fur/resource/event/group.mpp>
  #undef FUR_MACRO
  constexpr index count=FUR_INDEX;
  #undef FUR_INDEX

  constexpr auto at = meta::metafunction<guts::at_impl>;
  
  constexpr auto all = meta::transform(meta::unpack(meta::range_c<index,0,group::count>,
                                                    meta::make_tuple),
                                       at); }

namespace event::groups{
  #define FUR_MACRO(id,...) constexpr auto id = meta::type_c<group::id>;
  #include<fur/resource/event/group.mpp>
  #undef FUR_MACRO
}
                                       
namespace event::group{
  constexpr auto to_mask =
    [](auto group){
      constexpr auto map =
        meta::make_map(meta::make_pair(groups::structure,meta::long_c<StructureNotifyMask>),
                       meta::make_pair(groups::key_press,meta::long_c<KeyPressMask>),
                       meta::make_pair(groups::key_release,meta::long_c<KeyReleaseMask>),
                       meta::make_pair(groups::pointer_motion,meta::long_c<PointerMotionMask>),
                       meta::make_pair(groups::button_press,meta::long_c<ButtonPressMask>),
                       meta::make_pair(groups::button_release,meta::long_c<ButtonReleaseMask>),
                       meta::make_pair(groups::enter_window,meta::long_c<EnterWindowMask>),
                       meta::make_pair(groups::leave_window,meta::long_c<LeaveWindowMask>),   
                       meta::make_pair(groups::exposure,meta::long_c<ExposureMask>));
    return meta::at_key(map,group); }; }

namespace event::category{
  namespace guts{
    constexpr index index_offset = __COUNTER__+1;
    template<class> struct at_impl; }
  #define FUR_INDEX __COUNTER__-guts::index_offset
  #define FUR_MACRO(id,...)\
    struct id{};\
    namespace guts{\
      template<>\
      struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
        using type=id; }; }
  #include<fur/resource/event.mpp>
  #undef FUR_MACRO
  constexpr index count=FUR_INDEX;
  #undef FUR_INDEX

  constexpr auto at = meta::metafunction<guts::at_impl>;
  constexpr auto all = meta::transform(meta::unpack(meta::range_c<index,0,count>,
                                                    meta::make_tuple),
                                       at); }
                      
namespace event::categories{
  #define FUR_MACRO(id,...) constexpr auto id = meta::type_c<category::id>;
  #include<fur/resource/event.mpp>
  #undef FUR_MACRO
}

namespace event{

  constexpr auto to_group =
    [](auto event){
      constexpr auto map =
        meta::make_map(meta::make_pair(categories::configure,groups::structure),
                       meta::make_pair(categories::show,groups::structure),
                       meta::make_pair(categories::hide,groups::structure),
                       meta::make_pair(categories::key_press,groups::key_press),
                       meta::make_pair(categories::key_release,groups::key_release),
                       meta::make_pair(categories::pointer_motion,groups::pointer_motion),
                       meta::make_pair(categories::button_press,groups::button_press),
                       meta::make_pair(categories::button_release,groups::button_release),
                       meta::make_pair(categories::enter_window,groups::enter_window),
                       meta::make_pair(categories::leave_window,groups::leave_window),
                       meta::make_pair(categories::expose,groups::exposure));  
      return meta::at_key(map,event); }; 

  constexpr auto to_native =
    [](auto event){
      constexpr auto map =
        meta::make_map(meta::make_pair(categories::configure,meta::type_c<XConfigureEvent>),
                       meta::make_pair(categories::show,meta::type_c<XMapEvent>),
                       meta::make_pair(categories::hide,meta::type_c<XUnmapEvent>),
                       meta::make_pair(categories::key_press,meta::type_c<XKeyEvent>),
                       meta::make_pair(categories::key_release,meta::type_c<XKeyEvent>),
                       meta::make_pair(categories::pointer_motion,meta::type_c<XMotionEvent>),
                       meta::make_pair(categories::button_press,meta::type_c<XButtonEvent>),
                       meta::make_pair(categories::button_release,meta::type_c<XButtonEvent>),
                       meta::make_pair(categories::enter_window,meta::type_c<XCrossingEvent>),
                       meta::make_pair(categories::leave_window,meta::type_c<XCrossingEvent>),
                       meta::make_pair(categories::expose,meta::type_c<XExposeEvent>));
    return meta::at_key(map,event); };
      
  template<class category>
  using native_ = typename std::remove_cvref_t<decltype(to_native(meta::type_c<category>))>::type; }

template<class category>
struct event_{
  event::native_<category> native; };

namespace events{
  #define FUR_MACRO(id,...) using id = event_<event::category::id>;
  #include<fur/resource/event.mpp>
  #undef FUR_MACRO
}
  
namespace event{
  namespace native{ using any = XEvent; }
  using any = decltype(meta::unpack(meta::transform(category::all,meta::template_<event_>),
                                    meta::template_<std::variant> ))::type;
                                    
  std::optional<any> demux(const native::any &);
  std::vector<any> read(channel_ &); }

constexpr auto event_map =
  meta::make_map(
    meta::make_pair(categories::screen,meta::make_tuple()),
    meta::make_pair(
      categories::window,
      meta::make_tuple(event::categories::configure,
                       event::categories::show,
                       event::categories::hide,
                       event::categories::key_press,
                       event::categories::key_release,
                       event::categories::pointer_motion,
                       event::categories::button_press,
                       event::categories::button_release,
                       event::categories::enter_window,
                       event::categories::leave_window,
                       event::categories::expose)),
    meta::make_pair(resource::categories::glx_window,meta::make_tuple()));
    
constexpr auto to_events = [](auto r){ return meta::at_key(event_map,r); };
constexpr auto has_event = [](auto r,auto e){ return meta::contains(meta::at_key(event_map,r),e)(); }; 

constexpr auto to_groups =
  [](auto r){
    return meta::unique(meta::sort(meta::transform(to_events(r),event::to_group),
                                   meta::comparing(meta::typeid_))); };

namespace reactor{
  template<class event>
  using function = std::function<void(const event_<event> &)>;
  template<class event,class T>
  concept bool concept_ = requires(T v){ function<event>{v}; }; }

template<class event>
struct reactor_{
  reactor::function<event> function;

  void operator()(const event_<event> &e){
    if(function)
      function(e); } };




template<class group>
struct group_info{
  std::size_t size{0};

  auto mask()const noexcept{
    constexpr auto mask = event::group::to_mask(meta::type_c<group>)();     
    if(!size)
      return (decltype(mask))0;
    return mask; } };

template<class resource>
struct group_infos{
  static constexpr auto groups = to_groups(meta::type_c<resource>);
  using container_ =
    decltype(meta::unpack(meta::transform(groups,
                                          meta::template_<group_info>),
                          meta::template_<std::tuple>))::type;
  container_ container;

  template<class G>
  static constexpr auto index(){
    return meta::index_if(groups,meta::equal.to(meta::type_c<G>)).value(); }

  template<class G> constexpr const group_info<G> &at()const{ return std::get<index<G>()>(container); }
  template<class G> constexpr group_info<G> &at(){ return std::get<index<G>()>(container); }

  auto mask(auto g)const{ return at<typename decltype(g)::type>().mask(); }

};

    
template<class resource>
struct reactors{
  static constexpr auto events = to_events(meta::type_c<resource>);
  static constexpr auto groups = to_groups(meta::type_c<resource>);

  template<class E>
  static constexpr index event_index(){
    return meta::index_if(events,meta::equal.to(meta::type_c<E>)).value(); }
      
  using container_ =
    typename decltype(
      meta::unpack(
        meta::transform(events,
                        meta::compose(meta::template_<std::vector>,        
                                      meta::template_<reactor_>)),
        meta::template_<std::tuple>))::type;

  group_infos<resource> group_info;
  container_ container;

  template<class E>
  constexpr std::vector<reactor_<E>> &at()noexcept{ return std::get<event_index<E>()>(container); }
  template<class E>
  constexpr const std::vector<reactor_<E>> &at()const noexcept{ return std::get<event_index<E>()>(container); }    
  
  auto mask()const{
    return meta::fold(groups,
                      long{0},
                      [&](auto m,auto g){ return m|group_info.mask(g); }); }

  template<class E,class F>
  requires reactor::concept_<typename E::type,F> 
  void add(E e,F f){
    group_info.template at<typename decltype(event::to_group(e))::type>().size++;  
    at<typename E::type>().push_back({{f}}); }
  template<class E,class F>
  requires !reactor::concept_<typename E::type,F>   
  void add(E e,F f){}
                      
  template<class F>
  void add(F f){
    meta::for_each(events,[&](auto e){ add(e,f); }); }

  template<class event>
  requires has_event(meta::type_c<resource>,meta::type_c<event>)
  void react(const event_<event> &e){
    for(auto &x:at<event>())
      x(e); } };


namespace native{
  using id = XID;
  constexpr id none{None}; 

  using attrs = XWindowAttributes;

  namespace raw{
    using screen      = Screen *;
    using window      = Window;
    using visual_info = XVisualInfo;
    using visual      = Visual *;
    using colormap    = Colormap;
    using glx_context = GLXContext;
    using glx_window  = GLXWindow; } }

template<class>
struct native_{};

template<>
struct native_<category::screen>{
  index screen_number;
  native::raw::screen screen;
  native::raw::visual_info visual_info;
  native::raw::colormap colormap;
  native::raw::glx_context glx_context; };

template<>
struct native_<category::window>{
  native::raw::window window; };

template<>
struct native_<category::glx_window>{
  native::raw::glx_context glx_context; 
  native::raw::glx_window glx_window; };

namespace native{
  #define FUR_MACRO(id_,...) using id_ = native_<category::id_>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}

template<class>
struct request_{};

template<>
struct request_<category::screen>{
  std::optional<index> screen_number; };

template<>
struct request_<category::window>{
  std::optional<id::window> parent;
  std::optional<id::screen> screen; };

template<>
struct request_<category::glx_window>{
  id::window window; };

namespace request{
  #define FUR_MACRO(id,...) using id = request_<category::id>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}
      
template<class resource>
struct object_{
  channel_ &channel;
  request_<resource> request;
  native_<resource> native;
  reactors<resource> reactor; };

namespace object{
  #define FUR_MACRO(id,...) using id = object_<category::id>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}

native::attrs attrs(object::window &);
void select(object::window &,long);
void show(object::window &);
void hide(object::window &);
void move(object::window &,double,double);
void resize(object::window &,double,double);
void border(object::window &,const color &);
void background(object::window &,const color &);
void clear(object::window &);

void swap(object::glx_window &);
void current(object::glx_window &);
void release(object::glx_window &);

template<class R>
void react(object::window &o,R r){
  auto m_old=o.reactor.mask();
  o.reactor.add(r);
  auto m_new=o.reactor.mask();
  if(m_old!=m_new)
    select(o,m_new); }

struct objects{
  using container_ =
    decltype(meta::unpack(meta::transform(category::all,
                                          meta::compose(meta::template_<std::vector>,
                                                        meta::template_<object_>)),
                          meta::template_<std::tuple>))::type; 

  channel_ &channel;
  container_ container;
  id::screen default_screen;
  std::map<native::id,id::any> id;

  objects(channel_ &);
  ~objects();
  
  template<class c>
  constexpr auto &at()noexcept{
    return std::get<category::index(meta::type_c<c>)>(container); }
    
  template<class c>
  constexpr auto &at()const noexcept{
    return std::get<category::index(meta::type_c<c>)>(container); }
    
  template<class c>
  constexpr auto &at(id_<c> i){
    return at<c>().at(i.value); }
    
  template<class c>
  constexpr auto &at(id_<c> i)const{
    return at<c>().at(i.value); }  

  template<class category>
  id_<category> make(request_<category>); };

#define FUR_MACRO(id_,...)\
  extern template id::id_ objects::make(request::id_);
#include<fur/resource.mpp>
#undef FUR_MACRO

    
}

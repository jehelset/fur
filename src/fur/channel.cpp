#include<iostream>
#include<fur/channel.hpp>
namespace fur{

namespace channel{
  int error_handler(native_ di,XErrorEvent *ev){
    std::array<char,512> error_message{};
    XGetErrorText(di,ev->error_code,error_message.data(),error_message.size());
    std::cerr<<"xl.channel.error - "<<error_message.data()<<std::endl;
    return 0; }

  native_ open(const request_ &request){
    native_ native=XOpenDisplay(request);
    if(native == native::none)
      throw std::runtime_error{"fur.channel.open"};
    return native; }

  init::init(){ XSetErrorHandler(&error_handler); } }

const channel::init channel_::init = {};

channel_::channel_(channel::request_ request):
  native{channel::open(request)}
{}

channel_::~channel_(){}

channel::socket channel_::socket()const{ return XConnectionNumber(native); }
void channel_::flush()const{ XFlush(native); }
std::size_t channel_::pending()const{
  int n = XPending(native);
  if( n < 0 )
    throw std::runtime_error("fur.channel");
  return static_cast<std::size_t>(n); }

}

#include<fur/geometry.hpp>
namespace fur::geometry{

objects::objects(){}
objects::~objects(){}

auto deps(const objects &d,const id &i){
  std::set<id> D;
  std::queue<id> Q;
  Q.push(i);
  do{
    auto i=Q.front();
    Q.pop();
    D.insert(i);
    auto I_dep=d.at(i).dep;
    for(auto &i_dep:I_dep)
      Q.push(i_dep); }
  while(!Q.empty());
  return D; } 

template<op::concepts::term c>
void dep(objects &d,id i,const op_<c> &){}

template<op::concepts::unop c>
void dep(objects &d,id i,const op_<c> &o){
  d.at(o.oprn).dep.insert(i); }

template<op::concepts::binop c>
void dep(objects &d,id i,const op_<c> &o){
  for(auto j:o.oprn)
    d.at(j).dep.insert(i); }

void dep(objects &d,id i){
  return std::visit([&](auto &b){ return dep(d,i,b); },d.at(i).op); }

template<op::concepts::term c>
void undep(objects &d,id i,const op_<c> &){}

template<op::concepts::unop c>
void undep(objects &d,id i,const op_<c> &o){
  d.at(o.oprn).dep.erase(i); }

template<op::concepts::binop c>
void undep(objects &d,id i,const op_<c> &o){
  for(auto j:o.oprn)
    d.at(j).dep.insert(i); }

void undep(objects &d,id i){
  return std::visit([&](auto &o){ return undep(d,i,o); },d.at(i).op); }

void objects::assign(id i,op::any op){
  undep(*this,i);
  at(i).op = std::move(op);
  dep(*this,i);
  auto D = at(i).dep;
  for(auto d:D)
    at(d).reactor(event::assign{}); }

id objects::make(op::any op){
  id i{container.size()};
  container.push_back({std::move(op)});
  dep(*this,i);
  return i; }

namespace guts{
  struct eval_impl{
    const objects &object;

    template<op::concepts::term term>
    field operator()(const op_<term> &op)const{
      return op.oprn; }

    template<op::concepts::unop unop>
    field operator()(const op_<unop> &op)const{
      return meta::overload([](op::category::variable,auto o){ return o; })
                           (unop{},std::visit(*this,object.at(op.oprn).op)); }    
    
    template<op::concepts::binop binop>
    field operator()(const op_<binop> &op)const{
      if(op.oprn.empty())
        return meta::overload([](op::category::plus    ){ return 0.0; },
                              [](op::category::minus   ){ return 0.0; },
                              [](op::category::multiply){ return 1.0; },
                              [](op::category::divide  ){ return 1.0; })
                             (binop{});
      field l = std::visit(*this,object.at(op.oprn[0]).op);
      for(std::size_t i=1;i!=op.oprn.size();++i)
        meta::overload([&](op::category::plus,    auto r){ l += r; },
                       [&](op::category::minus,   auto r){ l -= r; },
                       [&](op::category::multiply,auto r){ l *= r; },
                       [&](op::category::divide,  auto r){ l /= r; })
                      (binop{},std::visit(*this,object.at(op.oprn[i]).op));
      return l; } }; }
      
field objects::eval(id i)const{ return std::visit(guts::eval_impl{*this},this->at(i).op); }

}

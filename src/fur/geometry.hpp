#pragma once
#include<fur/preamble.hpp>
namespace fur::geometry{

using field = double;
using id    = index;
using ids   = std::vector<index>;

namespace event::category{
  namespace guts{
    constexpr index index_offset=__COUNTER__+1;
    template<class> struct at_impl;
  }
  #define FUR_INDEX __COUNTER__-guts::index_offset
  #define FUR_MACRO(id,...)\
    struct id{};\
    namespace guts{\
      template<>\
      struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
        using type=id; }; }
  #include<fur/geometry/event.mpp>
  #undef FUR_MACRO
  constexpr index count=FUR_INDEX;
  #undef FUR_INDEX

  constexpr auto at = meta::metafunction<guts::at_impl>;

  constexpr auto all =
    meta::transform(meta::unpack(meta::range_c<index,0,count>,
                                 meta::make_tuple),
                    at);

  constexpr auto index = [](auto e){ return meta::index_if(all,meta::equal.to(e)).value(); }; }
                    
template<class cateogry>
struct event_{};

namespace event{
  #define FUR_MACRO(id,...) using id = event_<category::id>;
  #include<fur/geometry/event.mpp>
  #undef FUR_MACRO
}

namespace reactor{
  template<class category>
  using function = std::function<void(const event_<category> &)>;
  template<class category,class T>
  concept bool concept_ = requires(T v){ function<category>{v}; }; }

template<class category>
struct reactor_{
  reactor::function<category> function;

  void operator()(const event_<category> &e){
    if(function)
      function(e); } };

struct reactors{
  using container_ =
    typename decltype(meta::unpack(meta::transform(event::category::all,
                                                   meta::compose(meta::template_<std::vector>,
                                                                 meta::template_<reactor_>)),
                                   meta::template_<std::tuple>))::type; 
  container_ container;

  template<class E>
  constexpr auto &at()noexcept{
    return std::get<event::category::index(meta::type_c<E>)>(container); }

  template<class E>
  constexpr auto &at()const noexcept{
    return std::get<event::category::index(meta::type_c<E>)>(container); }    

  template<class category>
  void operator()(const event_<category> &e){
    for(auto &r:at<category>())
      r(e); }

  template<class E,class F>
  requires reactor::concept_<typename E::type,F> 
  void add(E e,F f){ at<typename E::type>().push_back({f}); }
  template<class E,class F>
  void add(E e,F f){}
  
  template<class F>
  void add(F f){
    meta::for_each(
      event::category::all,
      [&](auto e){ add(e,f); }); } };


namespace op::category{

  namespace guts{
    constexpr index index_offset=__COUNTER__+1;
    template<class> struct at_impl;
  }

  #define FUR_INDEX __COUNTER__-guts::index_offset
  #define FUR_MACRO(id,...)\
    struct id{};\
    namespace guts{\
      template<>\
      struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
        using type=id; }; }
  #include<fur/geometry/op.mpp>
  #undef FUR_MACRO
  constexpr index count=FUR_INDEX;
  #undef FUR_INDEX
  constexpr auto at = meta::metafunction<guts::at_impl>;

  constexpr auto all =
    meta::transform(meta::unpack(meta::range_c<index,0,category::count>,
                                 meta::make_tuple),
                    at); }

namespace op::categories{                    
  #define FUR_MACRO(id,...) constexpr auto id = meta::type_c<category::id>;
  #include<fur/geometry/op.mpp>
  #undef FUR_MACRO
}

namespace op::concepts{
  template<class category>
  concept bool unop =
    decltype(meta::find(meta::make_tuple(categories::variable),
                        meta::type_c<category>) != meta::nothing)::value;

  template<class category>
  concept bool binop =
    decltype(meta::find(meta::make_tuple(categories::plus,categories::minus,categories::multiply,categories::divide),
                        meta::type_c<category> ) != meta::nothing)::value;

  template<class category>
  concept bool nterm = unop <category> || binop<category>;

  template<class category>
  concept bool term = !nterm<category>; }

template<class category> struct op_{};
template<op::concepts::term c> struct op_<c>{ field oprn; };
template<op::concepts::unop c> struct op_<c>{ id oprn; };
template<op::concepts::binop c> struct op_<c>{ ids oprn; };  

namespace op{
  using any = decltype(meta::unpack(meta::transform(op::category::all,meta::template_<op_>),
                                    meta::template_<std::variant>))::type; }
                          
struct object{
  op::any op;
  std::set<id> dep;

  reactors reactor; };

struct objects{
  std::vector<object> container;

  objects();
  ~objects();

  auto &at(id i){ return container.at(i); }
  auto &at(id i)const{ return container.at(i); }

  void assign(id,op::any);
  id make(op::any);

  field eval(id)const; };

}

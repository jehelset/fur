#include<fur/context.hpp>
namespace fur{

context::context():
  channel{},
  resource{channel},
  geometry{},  
  keyboard{channel,resource},
  mouse{channel,resource}
{}

context::~context(){}
 
}

#pragma once
#include<fur/preamble.hpp>
#include<fur/channel.hpp>
#include<fur/resource.hpp>
#include<fur/geometry.hpp>
#include<fur/input/keyboard.hpp>
#include<fur/input/mouse.hpp>
namespace fur{

struct context{
  channel_ channel;
  resource::objects resource;
  geometry::objects geometry;
  input::keyboard::objects keyboard;
  input::mouse::objects mouse;

  context();
  ~context(); };

}

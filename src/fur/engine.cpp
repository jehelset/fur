#include<fur/engine.hpp>
namespace fur{

template<class E,class R>
requires resource::has_event(meta::type_c<R>,meta::type_c<E>)
void
  handle(context &c,const resource::event_<E> &e,resource::id_<R> i)
{ c.resource.at(i).reactor.react(e); }

template<class E,class R>
void
  handle(context &c,const resource::event_<E> &e,resource::id_<R> i)
{}

template<class E>
bool handle(context &c,const resource::event_<E> &e){
  auto it = c.resource.id.find(e.native.window);
  if(it!=std::end(c.resource.id))
    std::visit([&](auto r){ handle(c,e,r); },it->second);
  return true; }

bool engine::operator()(){
  auto event = resource::event::read(context.channel);
  for(std::size_t i=0;i!=event.size();++i)
    if(!std::visit([&](const auto &ev){ return handle(context,ev); },event[i]))
      return false;
  return true; }
  
engine::engine(fur::context &c):
  context{c}
{
  for(auto &ke:context.keyboard.container)
    ke.compile(c.channel); }

engine::~engine(){}

}

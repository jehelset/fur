#pragma once

namespace fur::resource{

namespace category{

  namespace guts{
    constexpr index index_offset=__COUNTER__+1;
    template<class> struct at_impl; }
    
  #define FUR_INDEX __COUNTER__-guts::index_offset
  #define FUR_MACRO(id,...)\
    struct id{};\
    namespace guts{\
      template<>\
      struct at_impl<meta::integral_constant<index,FUR_INDEX>>{\
        using type = id; }; }
  #include<fur/resource.mpp>
  #undef FUR_MACRO

  constexpr index count=FUR_INDEX;
  #undef FUR_INDEX
  
  constexpr auto at = meta::metafunction<guts::at_impl>;
  
  constexpr auto all = meta::transform(meta::unpack(meta::range_c<index,0,count>,
                                                    meta::make_tuple),
                                       at);

  constexpr auto index = [](auto c){ return meta::index_if(all,meta::equal.to(c)).value(); }; }
  
template<class category>
struct id_{
  index value;
  operator const index &()const noexcept{ return value; } };

namespace id{
  using any = decltype(meta::unpack(meta::transform(category::all,meta::template_<id_>),
                                    meta::template_<std::variant>))::type; }

namespace id{
  #define FUR_MACRO(id,...) using id = id_<category::id>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}

namespace native{
  using id = XID;
  constexpr id none{None}; 

  using attrs = XWindowAttributes;
  
  namespace raw{
    using screen      = Screen *;
    using window      = Window;
    using visual_info = XVisualInfo;
    using visual      = Visual *;
    using colormap    = Colormap;
    using glx_context = GLXContext;
    using glx_window = GLXWindow; } }
  
template<class>
struct native_{};

template<>
struct native_<category::screen>{
  index screen_number;
  native::raw::screen screen;
  native::raw::visual_info visual_info;
  native::raw::colormap colormap;
  native::raw::glx_context glx_context; };

template<>
struct native_<category::window>{
  native::raw::window window; };

template<>
struct native_<category::glx_window>{
  native::raw::glx_context glx_context; 
  native::raw::glx_window glx_window; };

namespace native{
  #define FUR_MACRO(id_,...) using id_ = native_<category::id_>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}
  
template<class>
struct request_{};

template<>
struct request_<category::screen>{
  std::optional<index> screen_number; };

template<>
struct request_<category::window>{
  id::screen screen; 
  std::optional<id::window> parent; };

template<>
struct request_<category::glx_window>{
  id::window window; };

namespace request{
  #define FUR_MACRO(id,...) using id = request_<category::id>;
  #include<fur/resource.mpp>
  #undef FUR_MACRO
}
  
} 

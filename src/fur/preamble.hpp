#pragma once
#include<algorithm>
#include<cstdint>
#include<functional>
#include<limits>
#include<map>
#include<optional>
#include<queue>
#include<set>
#include<tuple>
#include<type_traits>
#include<utility>
#include<variant>
#include<vector>
#include<range/v3/all.hpp>
#include<range/v3/iterator_range.hpp>

#include<boost/predef.h>
#include<boost/hana.hpp>
#include<X11/X.h>
#include<X11/Xdefs.h>
#include<X11/Xatom.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h>
#undef linux
namespace fur{

namespace meta = boost::hana;
using index = uint64_t;
namespace index_{ constexpr index none=std::numeric_limits<index>::max(); } 

struct color{
  using native = unsigned long;
  double r{0.0},g{0.0},b{0.0},a{1.0};

  native pixel()const{
    const double max=(double)(std::numeric_limits<uint8_t>::max());
    constexpr auto size=8*sizeof(uint8_t);
    return 
      (((unsigned long)(max*r))<<2*size)|
      (((unsigned long)(max*g))<<1*size)|
      (((unsigned long)(max*b))<<0*size); } }; 

}

#pragma once
#include<array>
#include<functional>
#include<map>
#include<set>
#include<vector>
#include<fur/preamble.hpp>
#include<fur/resource.hpp>
namespace fur::input::mouse{

using id = index;

constexpr index button_count = 10;
//using button_set=meta::range<count>;
using cross_rule=std::function<void(double,double)>;
using click_rule=std::function<void(double,double)>;
using click_rules=std::array<click_rule,button_count>;
using drag_rule=std::function<void(double,double)>;
using drag_rules=std::map<std::set<int>,drag_rule>;

struct object{
  cross_rule enter,leave;
  click_rules press,release;
  drag_rules drag; };

struct objects{
  channel_ &channel;
  resource::objects &resource;
  std::vector<object> container;
  
  auto &at(id i){ return container[i]; }
  auto &at(id i)const{ return container[i]; }
  
  objects(channel_ &,resource::objects &);
  ~objects();

  id make();
  void enable(id,resource::id::window);
  void disable(id,resource::id::window);
  void warp(id,resource::id::window,double,double);
  void grab(id,resource::id::window);
  void ungrab(); };


}

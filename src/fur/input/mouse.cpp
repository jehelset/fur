#include<fur/input/mouse.hpp>
namespace fur::input::mouse{

objects::objects(channel_ &c,resource::objects &r):
  channel{c},
  resource{r}
{}
objects::~objects(){}

id objects::make(){
  container.push_back({});
  return container.size()-1; }

template<std::size_t>
struct button_motion_mask{ constexpr unsigned int operator()()const{ return 0; } };
#define FUR_MACRO(index,number)\
  template<>\
  struct button_motion_mask<index>{\
    constexpr unsigned int operator()()const{ return Button##number##Mask; } };
FUR_MACRO(0,1)
FUR_MACRO(1,2)
FUR_MACRO(2,3)
FUR_MACRO(3,4)
FUR_MACRO(4,5)
#undef FUR_MACRO

constexpr auto buttons = meta::unpack(meta::range_c<index,0,button_count>,meta::make_tuple);

struct reactor{
  objects &object;
  mouse::id id;

  auto &mouse(){ return object.at(id); }

  template<class R,class E,class T,class... U>
  void click_dispatch(const R &r,const E &e,meta::tuple<T,U...> t){
    if(e.native.button != T::value+1)
      return click_dispatch(r,e,meta::make_tuple(U{}...));
    if(r[T::value])
      r[T::value]((double)e.native.x,(double)e.native.y); }

    
  template<class R,class E>
  void click_dispatch(const R &,const E &,meta::tuple<>){}

  void operator()(const resource::events::button_press &ev){
    click_dispatch(mouse().press,ev,buttons); }
    
  void operator()(const resource::events::button_release &ev){
    click_dispatch(mouse().release,ev,buttons); }
    
  void operator()(const resource::events::enter_window &ev){
    if(mouse().enter)
      mouse().enter((double)ev.native.x,(double)ev.native.y); }
      
  void operator()(const resource::events::leave_window &ev){
    if(mouse().leave)
      mouse().leave((double)ev.native.x,(double)ev.native.y); }

  unsigned int button_mask(meta::size_t<0>)const{ return Button1Mask; }
  unsigned int button_mask(meta::size_t<1>)const{ return Button2Mask; }
  unsigned int button_mask(meta::size_t<2>)const{ return Button3Mask; }
  unsigned int button_mask(meta::size_t<3>)const{ return Button4Mask; }
  unsigned int button_mask(meta::size_t<4>)const{ return Button5Mask; }
  template<std::size_t i>
  unsigned int button_mask(meta::size_t<i>)const{ return 0; }

  template<class E>
  void drag_set(const E &,std::set<int> &S,meta::tuple<>)const{}
  template<class E,class T,class... U>
  void drag_set(E &e,std::set<int> &S,meta::tuple<T,U...>)const{
    if(e.native.state&button_motion_mask<T::value>{}())
      S.insert(T::value);
    drag_set(e,S,meta::make_tuple(U{}...)); }
    
  void operator()(const resource::events::pointer_motion &ev){
    std::set<int> se;
    drag_set(ev,se,buttons);
    auto it=mouse().drag.find(se);
    if(it==mouse().drag.end()||!it->second)
      return;
    it->second((double)ev.native.x,(double)ev.native.y); } };

void objects::enable(id m,resource::id::window w){
  resource::react(resource.at(w),reactor{*this,m});
}
void objects::disable(id m,resource::id::window){}

void objects::warp(id i,resource::id::window w,double x,double y){
  XWarpPointer(
    channel,
    None,
    None,
    0,0,0,0,
    static_cast<int>(x),
    static_cast<int>(y)); }

void objects::grab(id i,resource::id::window w){
  XGrabPointer(
    channel,
    resource.at(w).native.window,
    False,
    ButtonPressMask|ButtonReleaseMask|PointerMotionMask,
    GrabModeSync,
    GrabModeAsync,
    None,
    None,
    CurrentTime); }
    
void objects::ungrab(){
  XUngrabPointer(channel,CurrentTime); }

}

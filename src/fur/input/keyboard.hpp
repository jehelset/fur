#pragma once
#include<memory>
#include<fur/preamble.hpp>
#include<fur/channel.hpp>
#include<fur/resource.hpp>
namespace fur::input::keyboard{

using function = std::function<void()>;

struct stroke{
  using direction_ = bool;
  using symbol_    = KeySym;
  static constexpr direction_ in{false},out{true};
  
  direction_ direction;
  symbol_ symbol; };

namespace strokes{
  struct constant{
    stroke::symbol_ symbol;
    stroke
      in{stroke::in,symbol},
      out{stroke::out,symbol}; };
  #define FUR_MACRO(id,native) static constexpr constant id{native};
  #include<fur/input/keyboard/stroke.mpp>
  #undef FUR_MACRO 
}

struct rule{
  std::vector<keyboard::stroke> stroke;
  keyboard::function function; };
using rules = std::vector<rule>;

namespace symbol{ using id = index; }

struct machine;

struct object{
  rules rule;
  std::unique_ptr<machine> machine_ptr;

  object(rules);
  object(object &&);
  object &operator=(object &&);
  ~object();

  object(const object &)=delete;
  object &operator=(const object &)=delete;

  void compile(channel_ &);
  void step(symbol::id); };

using id = index;

struct objects{
  channel_ &channel;
  resource::objects &resource;
  std::vector<object> container;

  auto &at(id i){ return container[i]; }
  auto &at(id i)const{ return container[i]; }

  id make(rules = {});
  
  void enable(id,resource::id::window);
  void disable(id,resource::id::window); };
  
}

#include<fur/input/keyboard.hpp>
#include<cog.hpp>
namespace fur::input::keyboard{

namespace symbol{
  using native = unsigned int;
  static constexpr native
    native_min = 8,
    native_max = 255,
    native_num = (255+1-8); }
id to_symbol(const resource::events::key_press &e){
  return e.native.keycode-symbol::native_min; }
id to_symbol(const resource::events::key_release &e){
  return (e.native.keycode-symbol::native_min)+symbol::native_num; }
id to_symbol(const channel_ &c,const stroke &s){
  symbol::native n=XKeysymToKeycode(c,s.symbol);
  return (s.direction==stroke::in)?(n-symbol::native_min):((n-symbol::native_min)+symbol::native_num); }

static const cog::alphabets::simple alphabet =
  []{
    cog::alphabets::simple a;
    a.resize(2*symbol::native_num);
    return a; }();

auto table(channel_ &channel,const rules &rule){
  using nfa_ = cog::graphs::nfas::semantic;
  nfa_ nfa{};

  for(auto &r:rule){
    nfa_ rule_nfa;
    for(auto &stroke:r.stroke){
      symbol::id symbol{to_symbol(channel,stroke)};
      rule_nfa = cog::graph::append(rule_nfa,cog::graph::literal<nfa_>({symbol})); }
    nfa = cog::graph::select(nfa,rule_nfa); }

  for(auto [index,state]:ranges::views::enumerate(nfa.terminal))
    for(auto transition:nfa.in(state))
      nfa(transition) = (cog::index)index;

  auto [dfa_to_nfa,nfa_to_dfa] = cog::graph::convert(nfa);
  using dfa_ = cog::graphs::dfas::semantic;
  dfa_ dfa;

  dfa.initial  = dfa_to_nfa.initial;
  dfa.terminal = dfa_to_nfa.terminal;

  dfa.states(dfa_to_nfa.n_states());

  for(auto transition:dfa_to_nfa.transitions()){
    auto &transitions = dfa_to_nfa(transition);
    if(ranges::adjacent_find(transitions,
                             std::not_equal_to{},
                             [&](auto transition){ return nfa(transition); }) !=
                             std::end(transitions))
      throw std::runtime_error("fur.keyboard - ambiguous rules");

    dfa.transition(dfa_to_nfa.in(transition),
                   dfa_to_nfa.symbol(transition),
                   dfa_to_nfa.out(transition),
                   nfa(transitions[0])); }

  auto [min_dfa,min_dfa_inv] = cog::graph::minimize_state(dfa);

  auto [min_alpha,min_alpha_inv] = cog::graph::minimize_alphabet(keyboard::alphabet,min_dfa);
  
  dfa_ tab_dfa;
  tab_dfa.initial  = min_dfa.initial;
  tab_dfa.terminal = min_dfa.terminal;
  tab_dfa.states(min_dfa.n_states());
  for(auto t:min_dfa.transitions())
    tab_dfa.transition(min_dfa.in(t),
                       *min_alpha(min_dfa.symbol(t)),
                       min_dfa.out(t),
                       dfa(min_dfa(t)[0]));

  return std::make_pair(min_alpha,cog::table{cog::simple(min_alpha_inv),tab_dfa}); }
    
struct machine{
      
  enum class result{ step, error }; 

  cog::alphabet_<std::optional<cog::symbol::id>> alphabet;
  cog::table table;
  cog::index state;

  void reset(){ state = table.initial; }

  std::pair<result,std::optional<index>> step(cog::symbol::id symbol){
    auto symbol_min = alphabet(symbol);
    if(!symbol_min.has_value())
      return {result::error,std::nullopt};
    auto next = table(state,*symbol_min);
    if(!next.has_value())
      return {result::error,std::nullopt};
    state = next->first;
    return {result::step,next->second}; }
    
  machine(channel_ &c,const rules &r){
    std::tie(alphabet,table) = keyboard::table(c,r);
    reset();
  } };

object::object(rules r):
  rule{std::move(r)},
  machine_ptr{}
{}
object::~object(){}

object::object(object &&)=default;
object &object::operator=(object &&)=default;

void object::compile(channel_ &channel){
  machine_ptr = std::make_unique<machine>(channel,rule); }
  
void object::step(symbol::id s){
  auto [kind,match] = machine_ptr->step({s});
  switch(kind){
    case machine::result::error: {
      machine_ptr->reset();
      break; }
    case machine::result::step: {
      if(match){
        if(rule[*match].function)
          rule[*match].function();
        machine_ptr->reset(); }
      break; } } }

id objects::make(rules r){
  container.push_back({std::move(r)});
  return container.size()-1; }

struct reactor{
  objects &object;
  keyboard::id id;

  template<class category>
  void impl(const resource::event_<category> &e){
    object.at(id).step(to_symbol(e)); }  
  void operator()(const resource::events::key_press &e){ impl(e); }
  void operator()(const resource::events::key_release &e){ impl(e); } };

void objects::enable(id ki,resource::id::window wi){
  auto &w=resource.at(wi);
  resource::react(w,reactor{*this,ki}); }
  //FIXME:
  // store map[ki][wi] = resource::reactor::ids_t
  
void objects::disable(id ,resource::id::window){}
  //NOTE:
  //  resoruce::reactor::ids_t i=map[ki][wi];
  //  auto &w=at(d.resource.object,wi);
  //  resource::unreact(w,i);
  
}

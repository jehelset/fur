#pragma once
#include<fur/preamble.hpp>
#include<fur/context.hpp>
namespace fur{

template<class category>
struct resource_{
  fur::context &context;
  resource::id_<category> id;

  operator resource::id_<category>()const noexcept{ return id; }
  
  auto &object(){ return context.resource.at(id); }
  auto &object()const{ return context.resource.at(id); }

  resource_(fur::context &c,resource::request_<category> r = {}):
    context{c},
    id{context.resource.make(std::move(r))}
   {} };

template<>
struct resource_<resource::category::window>{
  using category = resource::category::window;
  fur::context &context;
  resource::id_<category> id;
  
  operator resource::id_<category>()const noexcept{ return id; }
  
  auto &object(){ return context.resource.at(id); }
  auto &object()const{ return context.resource.at(id); }

  void clear(){ resource::clear(object()); }
  void show(){ resource::show(object()); }
  void hide(){ resource::hide(object()); }
  void move(double x,double y){ resource::move(object(),x,y); }
  void resize(double w,double h){ resource::resize(object(),w,h); }
  void border(color c){ resource::border(object(),c); }
  void background(color c){ resource::background(object(),c); }

  template<class... F>
  void react(F... f){ (resource::react(object(),f),...); }  

  resource_(fur::context &c,resource::request_<category> r = {}):
    context{c},
    id{context.resource.make(std::move(r))}
   {} };   


template<>
struct resource_<resource::category::glx_window>{
  using category = resource::category::glx_window;
  fur::context &context;
  resource::id_<category> id;
  
  operator resource::id_<category>()const noexcept{ return id; }
  
  auto &object(){ return context.resource.at(id); }
  auto &object()const{ return context.resource.at(id); }

  void swap(){ return resource::swap(object()); }
  void current(){ return resource::current(object()); }
  void release(){ return resource::release(object()); }

  resource_(fur::context &c,resource::request_<category> r = {}):
    context{c},
    id{context.resource.make(std::move(r))}
   {} };   

   
#define FUR_MACRO(id,...) using id = resource_<resource::category::id>;
#include<fur/resource.mpp>
#undef FUR_MACRO


// template<class category>
// struct control{
//   context_t &context;
//   id_<category> object;

//   operator const id_<category> &()const noexcept{ return object; }

//   object<category> &operator()(){ return at(context.geometry.object,object); }
//   const object<category> &operator()()const{ return at(context.geometry.object,object); }
//   object<category> *operator->(){ return &at(context.geometry.object,object); }
//   const object<category> *operator->()const{ return &at(context.geometry.object,object); }

//   field value()const{ return eval(context.geometry,object); }
  
//   control &operator=(object::value_tt<category> value){
//     asgn(context.geometry,object,std::move(value));
//     return *this;
//   }

//   template<class rhs>
//   requires std::is_same_v<rhs,category::var>
//   void asgn_op_impl(const control<rhs> &co){
//     (*this)=co.object;
//   }
//   template<class rhs>
//   requires !std::is_same_v<rhs,category::var>
//   void asgn_op_impl(const control<rhs> &co){
//     (*this)=co->value;
//   }
//   control &operator=(const control &co){
//     asgn_op_impl(co);
//     return (*this);
//   }  
  
//   template<class _=category>
//   requires std::is_same_v<category,category::var>
//   control &operator=(field v){
//     control<category::cst> co{context,v};
//     return (*this)=co.object;
//   }
  
//   template<class... value_arg_t>
//   control(context_t &c,value_arg_t &&... va):
//     context{c},
//     object{object::make<category>(c.geometry,object::value_tt<category>{std::forward<value_arg_t>(va)...})}
//   {}
// };

// inline control<category::var> &asgn(control<category::var> &co,const field &fi){
//   control<category::cst> co_oprn{co.context,fi};
//   return co=co_oprn;
// }
// template<class category>
// control<category> &asgn(control<category> &c,object::value_tt<category> v){
//   return c=std::move(v);
// }

// template<class category,class oprn_m>
// control<category> unop(control<oprn_m> oprn){
//   return {oprn.context,oprn.object}; }

// // #define FUR_MACRO(id)
// //   template<class oprn_m>
// //   control<id##_m> id(control<oprn_m> oprn){
// //     return {oprn.context,oprn.object};
// //   }
// // FUR_MACRO(var)
// // #undef FUR_MACRO

// template<class category,class lhs,class rhs>
// control<category> binop(control<lhs> lhs,control<rhs> rhs){
//   return {lhs.context,lhs.object,rhs.object}; }

// #define FUR_MACRO(id,tok)
//   template<class lhs,class rhs>
//   control<id##> operator tok(control<lhs> lhs,control<rhs> rhs){
//     return {lhs.context,lhs.object,rhs.object}; }
// FUR_MACRO(add,+) 
// FUR_MACRO(sub,-)
// FUR_MACRO(mul,*)
// FUR_MACRO(div,/)
// #undef FUR_MACRO

// template<class category,class... R>
// void react(control<category> &o,R &&... r){
//   react(o(),std::forward<R>(r)...); }

struct keyboard{
  fur::context &context;
  input::keyboard::id id;

  operator input::keyboard::id()const noexcept{ return id; }
   
  auto &object(){ return context.keyboard.at(id); }
  auto &object()const{ return context.keyboard.at(id); }

  auto &operator[](std::vector<input::keyboard::stroke> st){
    object().rule.push_back({std::move(st)});
    return object().rule.back().function; }

  auto &operator[](input::keyboard::stroke st){
    return (*this)[std::vector{st}]; }      

  void enable(resource::id::window w){ context.keyboard.enable(id,w); }
  void disable(resource::id::window w){ context.keyboard.disable(id,w); }
  
  keyboard(fur::context &c,input::keyboard::rules ru={}):
    context{c},
    id{context.keyboard.make(std::move(ru))}
  {} };

struct mouse{
  fur::context &context;
  input::mouse::id id;

  auto &object(){ return context.mouse.at(id); }
  auto &object()const{ return context.mouse.at(id); }

  auto &enter(){ return object().enter; }
  auto &leave(){ return object().leave; }
  auto &press(index i){ return object().press[i]; }
  auto &release(index i){ return object().release[i]; }
  auto &drag(std::set<int> s){ return object().drag[s]; }
  void enable(resource::id::window w){ context.mouse.enable(id,w); }
  void disable(resource::id::window w){ context.mouse.disable(id,w); }
  void warp(resource::id::window w,double x,double y){ context.mouse.warp(id,w,x,y); }
  void grab(resource::id::window w){ context.mouse.grab(id,w); }
  void ungrab(){ context.mouse.ungrab(); } 
  
  mouse(fur::context &c):
    context{c},
    id{context.mouse.make()}
  {} };
  
}

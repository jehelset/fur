#pragma once
#include<fur/context.hpp>
namespace fur{

struct engine{
  fur::context &context;

  bool operator()();

  engine(fur::context &);
  ~engine(); }; }

#include<exception>
#include<iostream>
#include<random>
#include<thread>

#include<fur.hpp>

int main(int,char **){
  try{
    fur::context context;
    
    fur::window window{context};
    fur::mouse mouse{context};

    bool done = false;

    mouse.press(0) = [&](auto,auto){ window.background(fur::color{0.0,1.0,0.0}); window.clear(); };
    mouse.press(1) = [&](auto,auto){ done = true; };
    mouse.press(2) = [&](auto,auto){ window.background(fur::color{1.0,0.0,0.0}); window.clear(); };

    fur::engine engine{context};
    
    window.show();
    mouse.enable(window);
    
    while(engine() && !done);

  }catch(const std::exception &e){
    std::cerr<<e.what()<<std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

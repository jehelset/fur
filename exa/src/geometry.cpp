#include<exception>
#include<iostream>
#include<random>
#include<thread>

#include<fur.hpp>

int main(int,char **){
  try{
    fur::context context;
    
    fur::window window{context};
//    fur::glx_window window_glx{context,{window}};

    // fur::constant margin{context,0.95};
    // {
    //   w(window_glx) = margin * w(window);
    //   h(window_glx) = margin * h(window);
    //   x(window_glx) = (1.0 - margin) * w(window) / 2.0;
    //   y(window_glx) = (1.0 - margin) * h(window) / 2.0; }

    // fur::keyboard keyboard{context};
    // fur::mouse mouse{context};

  }catch(const std::exception &e){
    std::cerr<<e.what()<<std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

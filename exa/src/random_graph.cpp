#include<sys/wait.h>
#include<sys/stat.h> 
#include<fcntl.h>
#include<cstdio>
#include<exception>
#include<iostream>
#include<random>
#include<thread>

#include<boost/filesystem.hpp>
#include<cog.hpp>
#include<gruff/layout.hpp>
#include<fur.hpp>

#include<glm/glm.hpp>

using namespace std::string_literals;
namespace chr = std::chrono;
namespace thr = std::this_thread;
namespace fs  = boost::filesystem;

namespace app{
  using dfa_ = cog::graphs::dfas::simple;

  dfa_ default_dfa(){
    cog::alphabets::simple al;
    cog::graphs::dfas::simple dfa;
    auto s0 = dfa.state();
    auto s1 = dfa.state();
    auto s2 = dfa.state();
    auto y0 = al.symbol();
    auto y1 = al.symbol();
    dfa.initial  = s0;
    dfa.terminal = {s1,s2};
    dfa.transition(s0,y0,s1);
    dfa.transition(s0,y1,s2);
    dfa.transition(s1,y0,s0);
    dfa.transition(s2,y1,s1);
    return dfa; }  

  
  struct arg{ int argc; char **argv, **envp; };
 
  const glm::vec4
    state_off{1.0,1.0,1.0,0.05},
    state_error{1.0,0.2,0.2,1.0},
    state_term{0.1,0.8,0.8,1.0},
    state_on{0.2,1.0,0.2,1.0};

  const glm::vec4
    transition_off{1.0,1.0,1.0,0.05},
    transition_prev{0.4,1.0,0.4,0.4},
    transition_next{0.2,1.0,0.2,0.1};

  std::random_device random_device{};
  std::mt19937 random_generator{random_device()};

  namespace data{
    struct state__{
      glm::vec2 position;
      glm::vec4 mode;
      std::uniform_int_distribution<std::size_t> dist; };
    struct transition__{
      std::vector<glm::vec2> position;
      glm::vec4 mode; };
    struct graph__{
      glm::vec2 corner,extent; };
    using graph_ = cog::graphs::dfa<state__,transition__,graph__>;

    graph_ graph(const dfa_ &dfa){
      auto layout = gruff::layout_(gruff::graph::simple(dfa.guts));

      graph_ graph;

      graph.initial  = dfa.initial;
      graph.terminal = dfa.terminal;
      graph().corner = { layout()[0].x,layout()[0].y },
      graph().extent = { std::fabs(layout()[1].x - layout()[0].x),
                         std::fabs(layout()[1].y - layout()[0].y) };

      auto normalize = [&](auto &pos){
        return glm::vec2{(pos.x - graph().corner[0]) / graph().extent[0] - 0.5,
                (pos.y - graph().corner[1]) / graph().extent[1] - 0.5}; };
                
      for(auto state:dfa.states())
        graph.state(state__{normalize(layout(state)()),state_off});
      for(auto transition:dfa.transitions())
        graph.transition(
          dfa.in(transition),
          dfa.symbol(transition),
          dfa.out(transition),
          transition__{
            layout(transition)()
            |ranges::views::transform(normalize)
            |ranges::to<std::vector>,
            transition_off}); //NOTE: add points - jeh
          
      for(auto state:graph.states()){
        if(graph.out(state).size()<=1)
          continue;
        graph(state).dist =
          std::uniform_int_distribution{0ul,static_cast<std::size_t>(graph.out(state).size()-1ul)}; }

      return graph; } }
      
  struct data_{
    dfa_ dfa;
    data::graph_ graph;
    cog::graph::state::id state;

    data_(const dfa_ &g = default_dfa()):
      dfa{g},
      graph{data::graph(dfa)},
      state{graph.initial} //NOTE: make dfa-graph-initial optional
    {}

    void enter(auto state,bool error = false){
      this->state = state;
      if(!error){
        if(ranges::find(dfa.terminal,state)==ranges::end(dfa.terminal))
          graph(state).mode = state_on;
        else
          graph(state).mode = state_term;
      }else
        graph(state).mode = state_error;
      for(auto transition:graph.out(state))
        graph(transition).mode = transition_next; }

    void leave(auto state){
      graph(state).mode = state_off;
      for(auto transition:graph.in(state))
        graph(transition).mode = transition_off; }

    void travel(auto transition){
      leave(graph.in(transition));
      enter(graph.out(transition));
      graph(transition).mode = transition_prev; }

    void reset(bool error = false){
      for(auto state:graph.states())
        graph(state).mode = state_off;
      for(auto transition:graph.transitions())
        graph(transition).mode = transition_off;
      if(!graph.empty()){
        enter(graph.initial,error);
        state = graph.initial; } }

    std::optional<cog::graph::transition::id> next(auto state,auto symbol){
      return graph(state,symbol); }
    std::optional<cog::graph::transition::id> next(auto state){
      auto &out = graph.out(state);
      switch(out.size()){
        case 0: return std::nullopt; 
        case 1: return out[0];
        default: return out[graph(state).dist(random_generator)]; } }

    
    void step(auto... symbol){
      if(auto transition = next(state,symbol...))
        travel(*transition);
      else
        reset(true); }
        
  };

  namespace gl{
  
    inline std::string errstr(){ return {reinterpret_cast<const char*>(gluErrorString(glGetError()))}; }

    inline void check_error(){ 
      auto error=glGetError();
      if(error!=GL_NO_ERROR)
        throw std::runtime_error(errstr()); }
        
    namespace shader{
      using type_t=GLenum;
      using source_t=std::string;
      
      #define FUR_MACRO(id,ty)\
        struct id##_m{\
          static constexpr type_t type=ty; };
      FUR_MACRO(vertex,GL_VERTEX_SHADER)
      FUR_MACRO(fragment,GL_FRAGMENT_SHADER)      
      #undef FUR_MACRO

      struct base_t{
        GLuint id;

        operator GLuint()const noexcept{ return id; }

        base_t(type_t type,const source_t &src):
          id{glCreateShader(type)}
        {
          const char *c_str=src.c_str();
          glShaderSource(id,1,&c_str,0);
          glCompileShader(id);
          {
            int ret;
            glGetShaderiv(id,GL_COMPILE_STATUS,&ret);
            if(ret!=GL_TRUE){
              int
                max_length=2048,
                actual_length=0;
              std::vector<char> log(2048);
              glGetShaderInfoLog(id,max_length,&actual_length,log.data());
              log[actual_length]='\0';
              throw std::runtime_error(std::string{"app.gl - shader compilation error: "}+log.data());
            }
          }
        }
      };

    }

    template<class shader_m>
    struct shader_tt:shader::base_t{
      using shader::base_t::operator GLuint;
      shader_tt(const shader::source_t &src):
        shader::base_t{shader_m::type,src.c_str()}
      {}
    };
    namespace shader{
      #define FUR_MACRO(id,...) using id##_t=shader_tt<id##_m>;
      FUR_MACRO(vertex,GL_VERTEX_SHADER)
      FUR_MACRO(fragment,GL_FRAGMENT_SHADER)
      #undef FUR_MACRO
    }

    struct program_t{
      GLuint id;

      operator GLuint()const noexcept{ return id; }

      program_t():
        id{glCreateProgram()}
      {} };

    using location_t=GLint;
    inline location_t attr_location(program_t &pr,const std::string &id){
      location_t ul=glGetAttribLocation(pr,id.c_str());
      check_error();
      return ul;
    }    
    inline location_t uniform_location(program_t &pr,const std::string &id){
      location_t ul=glGetUniformLocation(pr,id.c_str());
      check_error();
      return ul;
    }
    template<class shader_m>
    void attach(program_t &pr,shader_tt<shader_m> &sh){
      glAttachShader(pr,sh);
    }
    inline void link(program_t &pr){
      glLinkProgram(pr);
      {
        int ret;
        glGetProgramiv(pr,GL_LINK_STATUS,&ret);
        if(ret!=GL_TRUE){
          int max_length = 2048;
          int actual_length = 0;
          std::vector<char> log(2048);
          glGetProgramInfoLog(pr,max_length,&actual_length,log.data());
          log[actual_length]='\0';
          throw std::runtime_error(std::string{"app.gl.program - link error: "}+log.data());
        }
      }
    }
    void use(program_t &pr){
      glUseProgram(pr);
    }

    struct viewport_t{
      GLint x,y;
      GLuint w,h;

      double aspect()const{ return w/h; } };
      
    void use(viewport_t &v){
      glViewport(v.x,v.y,v.w,v.h); }

    namespace vertex_buffer{
      using target_t=GLenum;
      #define FUR_MACRO(id,ta) struct id##_m{ static constexpr target_t target{ta}; };
      FUR_MACRO(array,GL_ARRAY_BUFFER)
      FUR_MACRO(element_array,GL_ELEMENT_ARRAY_BUFFER)
      #undef FUR_MACRO
    }    
    namespace vertex_buffer{
      struct base_t{
        GLuint id;

        operator GLuint()const noexcept{ return id; }
        base_t(){ glGenBuffers(1,&id); }
        virtual ~base_t(){ glDeleteBuffers(1,&id); } }; }
        
    template<class vertex_buffer_m>
    struct vertex_buffer_tt:vertex_buffer::base_t{
      using vertex_buffer::base_t::operator GLuint;
      vertex_buffer_tt():
        vertex_buffer::base_t{}
      {

      }

    };

    template<class vertex_buffer_m>
    void bind(vertex_buffer_tt<vertex_buffer_m> &b){
      glBindBuffer(vertex_buffer_m::target,b);
    }

    using usage_t=GLenum;

    #define FUR_MACRO(id,val) constexpr usage_t id=val;
    FUR_MACRO(stream_draw,GL_STREAM_DRAW)
    FUR_MACRO(stream_read,GL_STREAM_READ)
    FUR_MACRO(stream_copy,GL_STREAM_COPY)
    FUR_MACRO(static_draw,GL_STATIC_DRAW)
    FUR_MACRO(static_read,GL_STATIC_READ)
    FUR_MACRO(static_copy,GL_STATIC_COPY)
    FUR_MACRO(dynamic_draw,GL_DYNAMIC_DRAW)
    FUR_MACRO(dynamic_read,GL_DYNAMIC_READ)
    FUR_MACRO(dynamic_copy,GL_DYNAMIC_COPY)
    #undef FUR_MACRO
    
    template<class vertex_buffer_m,class T>
    void data(vertex_buffer_tt<vertex_buffer_m> &b,const std::vector<T> &d,usage_t u=dynamic_draw){
      glBufferData(vertex_buffer_m::target,sizeof(T)*d.size(),d.data(),GL_STATIC_DRAW);
    }

    namespace vertex_buffer{
      #define FUR_MACRO(id,...) using id##_t=vertex_buffer_tt<id##_m>;
      FUR_MACRO(array,GL_ARRAY_BUFFER)
      FUR_MACRO(element_array,GL_ELEMENT_ARRAY_BUFFER)
      #undef FUR_MACRO
    }

    struct vertex_array_t{
      GLuint id;

      operator GLuint()const noexcept{ return id; }

      vertex_array_t(){ glGenVertexArrays(1,&id); }
      ~vertex_array_t(){ glDeleteVertexArrays(1,&id); } };
      
    void bind(const vertex_array_t &va){
      glBindVertexArray(va);
    }
    void enable_vertex_attr_array(const vertex_array_t &va,GLuint uniform_location){
      glEnableVertexAttribArray(uniform_location);
    }
    void vertex_attr_ptr(
      const vertex_array_t &va,
      GLint uniform_location,
      GLint size,
      GLenum type,
      GLboolean normalized=GL_FALSE,
      GLsizei stride=0,
      const GLvoid *pointer=nullptr
    ){ glVertexAttribPointer(uniform_location,size,type,normalized,stride,pointer); }
  }
  struct gl_t{
    gl::vertex_buffer::array_t state_vertex;
    gl::vertex_buffer::array_t state_mode;
    std::size_t state_vertex_size;
    gl::vertex_array_t state_vertex_array;

    gl::vertex_buffer::array_t transition_vertex;
    gl::vertex_buffer::array_t transition_mode;
    std::size_t transition_vertex_size;
    gl::vertex_array_t transition_vertex_array;

    gl::viewport_t viewport;

    gl::location_t
      camera_pos_location,
      camera_zoom_location,
      vertex_pos_location,
      vertex_mode_location;

    gl::shader::vertex_t
      shader_vertex_state,
      shader_vertex_transition;
    gl::shader::fragment_t
      shader_fragment_state,
      shader_fragment_transition;
    gl::program_t
      program_state,
      program_transition;
    
    float camera_zoom;
    glm::vec2 camera_pos;

    void clear(double r=0.0,double g=0.0,double b=0.0,double a=1.0){
      glClearColor(r,g,b,a);
      glClear(GL_COLOR_BUFFER_BIT);
    }
    void draw(){
      clear();
      gl::use(viewport);
      
      gl::use(program_transition);
      gl::bind(transition_vertex_array);
      glDrawArrays(GL_LINES,0,transition_vertex_size);
      
      gl::use(program_state);
      gl::bind(state_vertex_array);
      glDrawArrays(GL_POINTS,0,state_vertex_size);

    }

    static constexpr const float camera_zoom_coef=1.05;
    void zoom(float z){
      gl::use(program_state);
      glUniform1f(camera_zoom_location,camera_zoom=z);
      gl::use(program_transition);
      glUniform1f(camera_zoom_location,camera_zoom=z);
    }
    void zoom_out(){
      move((float)(1.0/camera_zoom_coef)*camera_pos);
      zoom(camera_zoom/camera_zoom_coef);
    }
    void zoom_in(){
      move(camera_zoom_coef*camera_pos);
      zoom(camera_zoom*camera_zoom_coef);
    }
    void move(glm::vec2 camera_pos_new){
      camera_pos = camera_pos_new;
      gl::use(program_state);
      glUniform2f(camera_pos_location,camera_pos[0],camera_pos[1]);
      gl::use(program_transition);
      glUniform2f(camera_pos_location,camera_pos[0],camera_pos[1]);      
    }
    void displace(glm::vec2 camera_pos_del){
      move(camera_pos+camera_pos_del);
    }
    
    void upload_mode(data_ &d){
      std::vector<glm::vec4> state_mode_local(d.graph.n_states());
      for(auto s:d.graph.states())
        state_mode_local[s] = d.graph(s).mode;
      gl::bind(state_mode);
      gl::data(state_mode,state_mode_local,gl::dynamic_draw);

      std::vector<glm::vec4> transition_mode_local;
      for(auto t:d.graph.transitions()){
        auto &m = d.graph(t).mode;
        auto &s = d.graph(t).position;
        for(std::size_t i = 0;i + 1 < s.size();++i){
          (void)i;
          transition_mode_local.push_back(m);
          transition_mode_local.push_back(m); } }
      gl::bind(transition_mode);
      gl::data(transition_mode,transition_mode_local,gl::dynamic_draw);      
    }
    void upload(data_ &d){
      gl::bind(state_vertex);

      std::vector<glm::vec2> position(d.graph.n_states());
      for(auto s:d.graph.states())
        position[s] = d.graph(s).position;      
      gl::data(state_vertex,position,gl::static_draw);
      state_vertex_size=d.graph.n_states();

      std::vector<glm::vec2> transition;
      for(auto t:d.graph.transitions()){
        auto &s = d.graph(t).position;
        for(std::size_t i = 0;i + 1 < s.size();++i){
          transition.push_back(s[i]);
          transition.push_back(s[i+1]); } }
      gl::bind(transition_vertex);
      gl::data(transition_vertex,transition,gl::static_draw);
      transition_vertex_size = transition.size();

      upload_mode(d);
    }
    
    gl_t():
      state_vertex{},
      state_mode{},
      state_vertex_size{0},
      state_vertex_array{},
      transition_vertex{},
      transition_mode{},
      transition_vertex_size{0},
      transition_vertex_array{},
      viewport{0,0,640,480},
      camera_pos_location{0},
      camera_zoom_location{1},
      vertex_pos_location{2},
      vertex_mode_location{3},
      shader_vertex_state{
        "#version 450\n"
        "layout(location=0) uniform vec2 camera_pos;\n"
        "layout(location=1) uniform float camera_zoom;\n"
        "layout(location=2) in vec2 vertex_pos;\n"
        "layout(location=3) in vec4 vertex_color_in;\n"
        "out vec4 vertex_color_out;\n"
        "void main(){\n"
        "  vertex_color_out=vertex_color_in;\n"
        "  gl_Position=vec4(camera_pos+camera_zoom*vertex_pos,0.0,1.0);\n"
        "  gl_PointSize=5.0;\n"
        "}\n"
      },
      shader_vertex_transition{
        "#version 450\n"
        "layout(location=0) uniform vec2 camera_pos;\n"
        "layout(location=1) uniform float camera_zoom;\n"
        "layout(location=2) in vec2 vertex_pos;\n"
        "layout(location=3) in vec4 vertex_color_in;\n"
        "out vec4 vertex_color_out;\n"
        "void main(){\n"
        "  vertex_color_out=vertex_color_in;\n"
        "  gl_Position=vec4(camera_pos+camera_zoom*vertex_pos,0.0,1.0);\n"
        "}\n"
      },      
      shader_fragment_state{
        "#version 450\n"
        "in vec4 vertex_color_out;\n"
        "out vec4 color;\n"
        "void main(){\n"
        "  color=vertex_color_out;\n" 
        "}\n"
      },
      shader_fragment_transition{
        "#version 450\n"
        "in vec4 vertex_color_out;\n"
        "out vec4 color;\n"
        "void main(){\n"
        "  color=vertex_color_out;\n" 
        "}\n"
      },      
      program_state{},
      program_transition{},
      camera_zoom{1.0},
      camera_pos{0.0,0.0}
    {
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
      gl::bind(state_vertex_array);

      gl::enable_vertex_attr_array(state_vertex_array,vertex_pos_location);
      gl::enable_vertex_attr_array(state_vertex_array,vertex_mode_location);
      
      gl::bind(state_vertex);      
      gl::vertex_attr_ptr(state_vertex_array,vertex_pos_location,2,GL_FLOAT);

      gl::bind(state_mode);
      gl::vertex_attr_ptr(state_vertex_array,vertex_mode_location,4,GL_FLOAT);

      gl::bind(transition_vertex);
      
      gl::bind(transition_vertex_array);
      gl::enable_vertex_attr_array(transition_vertex_array,vertex_pos_location);
      gl::enable_vertex_attr_array(transition_vertex_array,vertex_mode_location);
      
      gl::bind(transition_vertex);      
      gl::vertex_attr_ptr(transition_vertex_array,vertex_pos_location,2,GL_FLOAT);

      gl::bind(transition_mode);
      gl::vertex_attr_ptr(transition_vertex_array,vertex_mode_location,4,GL_FLOAT);      
    
      gl::attach(program_state,shader_vertex_state);
      gl::attach(program_state,shader_fragment_state);
      gl::link(program_state);
      gl::use(program_state);
      glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

      gl::attach(program_transition,shader_vertex_transition);
      gl::attach(program_transition,shader_fragment_transition);
      gl::link(program_transition);

      zoom(1.0);
      move({0.0,0.0}); } };
      
  struct gui{
    app::arg &arg;
    app::data_ &data;
    
    fur::context context{};
    fur::window window_slab{context};
    fur::glx_window glx_window_slab{context,{window_slab}};
    fur::keyboard keyboard{context};
    fur::mouse mouse{context};

    gui(app::arg &a,app::data_ &d):
      arg{a},
      data{d}
    { window_slab.background({0.0,0.0,0.0}); }

    void init(gl_t &gl){
      data.reset();
      gl.upload(data);
    }
    void draw(gl_t &gl){
      gl.upload_mode(data);    
      gl.draw();
      glx_window_slab.swap(); }
    void reset(gl_t &gl){
      data.reset();
      draw(gl);
    }
    void step(gl_t &gl,auto... symbol){
      data.step(symbol...);
      draw(gl);
    }

    void run(){
      bool quit=false;
      
      glx_window_slab.current();
      gl_t gl{};
      
      init(gl);

      window_slab.react(
        [&](const fur::resource::events::configure &ev){
          gl.viewport.w = ev.native.width;
          gl.viewport.h = ev.native.height; },
        [&](const fur::resource::events::expose &){
          draw(gl); });
      {
        using namespace fur::input::keyboard::strokes;
        keyboard[q.in] = [&]{ quit=true; };
        keyboard[e.in] = [&]{ step(gl); };
        keyboard[d0.in] = [&]{ step(gl,cog::symbol::id{0}); };
        keyboard[d1.in] = [&]{ step(gl,cog::symbol::id{1}); };
        keyboard[r.in] = [&]{ reset(gl); };
        keyboard[x.in] = [&]{ data = app::data_{}; reset(gl); };
        keyboard[c.in] =
          [&]{
            auto nfa = cog::graph::convert(data.dfa);
            nfa = cog::graph::append(nfa,nfa);
            auto [dfa_to_nfa,nfa_to_dfa] = cog::graph::convert(nfa);
            data = app::data_{cog::simple(dfa_to_nfa)};
            init(gl);
            draw(gl); };
        keyboard[m.in] =
          [&]{
            auto [min_dfa,min_dfa_inv] = cog::graph::minimize_state(cog::semantic_(data.dfa));
            data = app::data_{cog::simple(min_dfa)};
            init(gl);
            draw(gl); };                        
        keyboard[f.in] = [&]{ reset(gl); };
      }

      struct mouse_state{
        double
          x_init,y_init,
          x_last,y_last; };
      auto ms=std::make_shared<mouse_state>();
      mouse.press(0) =
        [&,ms](double x,double y){
          ms->x_init=ms->x_last=x;
          ms->y_init=ms->y_last=y; };
      mouse.drag({0}) =
        [&,ms](double x,double y){
          double
            x_del=(x-ms->x_last)/(double)(gl.viewport.w),
            y_del=(y-ms->y_last)/(double)(gl.viewport.h);
          ms->x_last=x;
          ms->y_last=y;
          gl.displace({-2.0*x_del*gl.viewport.aspect(),2.0*y_del});
          draw(gl); };
      mouse.release(0) =
        [&](double x_fini,double y_fini){
          mouse.warp(window_slab,ms->x_init-x_fini,ms->y_init-y_fini); };
      
      mouse.press(3)=[&](double,double){ gl.zoom_in(); draw(gl); };
      mouse.press(4)=[&](double,double){ gl.zoom_out(); draw(gl); };
      
      fur::engine engine{context};
      mouse.enable(window_slab);
      keyboard.enable(window_slab);
      window_slab.show();
      while(!quit&&engine()); } }; }

int main(int argc, char **argv, char **envp){
  try{
    app::arg arg{argc,argv,envp};
    app::data_ data{};
    app::gui gui{arg,data};
    gui.run();
  }catch(const std::exception &ex){
    std::cerr<<ex.what()<<std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

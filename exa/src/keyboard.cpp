#include<exception>
#include<iostream>
#include<random>
#include<thread>

#include<fur.hpp>

int main(int,char **){
  try{
    fur::context context;
    
    fur::window window{context};
    fur::keyboard keyboard{context};

    bool done = false;
    {
      using namespace fur::input::keyboard::strokes;
      keyboard[{r.in,r.out}] = [&]{ window.background(fur::color{1.0,0.0,0.0}); window.clear(); };
      keyboard[{g.in,g.out}] = [&]{ window.background(fur::color{0.0,1.0,0.0}); window.clear(); };
      keyboard[{b.in,b.out}] = [&]{ window.background(fur::color{0.0,0.0,1.0}); window.clear(); };
      keyboard[{q.in}]       = [&]{ done = true; }; }

    fur::engine engine{context};

    window.show();
    keyboard.enable(window);
    
    while(engine() && !done);

  }catch(const std::exception &e){
    std::cerr<<e.what()<<std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

#include<exception>
#include<iostream>
#include<random>
#include<thread>

#include<fur.hpp>

int main(int,char **){
  try{
    fur::context context{};

    fur::window window{context};

    window.show();
    context.channel.flush();
    char c;
    std::cin>>c;
  }catch(const std::exception &e){
    std::cerr<<e.what()<<std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
